package kz.askar.binder1;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("My", "Service onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("My", "Service onStart");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("My", "Service onBind");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("My", "Service onUnbind");
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d("My", "Service onRebind");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("My", "Service onDestroy");
    }

    class MyBinder extends Binder{
        public MyService getService(){
            return MyService.this;
        }
    }


    public void startTask(String action){
        Intent intent = new Intent(action);
        intent.putExtra("done", "Done");
        sendBroadcast(intent);


        NotificationCompat.Builder nBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.presence_audio_away)
                        .setContentTitle("Android lesson")
                        .setContentText(action);
        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, nBuilder.build());


    }
}
