package kz.askar.binder1;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ServiceConnection sConn;
    MyService myService;
    boolean binded = false;

    TextView resultTV;
    EditText actionET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultTV = (TextView)findViewById(R.id.result);
        actionET = (EditText) findViewById(R.id.actionET);

        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("My", "Service connected");
                binded = true;

                myService = ((MyService.MyBinder)binder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d("My", "Service disconnected");
                binded = false;
                myService = null;
            }
        };

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                resultTV.setText(intent.getStringExtra("done"));
            }
        };
        IntentFilter filter = new IntentFilter("sdu.myaction");
        registerReceiver(br, filter);
    }

    public void start(View v){
        Log.d("My", "Try to start");
        Intent intent = new Intent(this, MyService.class);
        startService(intent);
    }

    public void stop(View v){
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
    }

    public void bind(View v){
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, sConn, BIND_AUTO_CREATE);
    }

    public void unbind(View v){
        if(binded){
            unbindService(sConn);
            binded = false;
            myService = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind(null);
    }

    public void task(View v){
        if(binded && myService!=null) {
            myService.startTask(actionET.getText().toString());
        }
    }
}
