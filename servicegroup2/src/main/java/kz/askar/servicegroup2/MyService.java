package kz.askar.servicegroup2;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    boolean stop = false;

    public MyService() {
        Log.d("My", "Service: constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("My", "Service: onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        Log.d("My", "Service: onStartCommand #"+startId);

        if(flags == START_FLAG_REDELIVERY){
            Log.d("My", "Redelivery");
        }

        int till = intent.getIntExtra("till", 0);
        int time = intent.getIntExtra("time", 0);
//        final PendingIntent pi = intent.getParcelableExtra("pi");

//        try {
//            pi.send(0);
//        } catch (PendingIntent.CanceledException e) {
//            e.printStackTrace();
//        }

        final int tillToThread = till;
        final int timeToThread = time;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<tillToThread;i++){
                    if(stop) break;
                    Log.d("My", "Service #"+startId+" task "+i);
//                    Intent pendingResultIntent = new Intent();
//                    pendingResultIntent.putExtra("counter", i);
//                    try {
//                        pi.send(MyService.this, 1, pendingResultIntent);
//                    } catch (PendingIntent.CanceledException e) {
//                        e.printStackTrace();
//                    }

                    Intent broadcastResultIntent = new Intent("sdu.myaction");
                    broadcastResultIntent.putExtra("counter", i);
                    sendBroadcast(broadcastResultIntent);

                    try {
                        Thread.sleep(timeToThread);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf(startId);
            }
        }).start();

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop = true;
        Log.d("My", "Service: onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
