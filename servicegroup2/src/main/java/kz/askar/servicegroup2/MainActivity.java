package kz.askar.servicegroup2;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int REQUEST_CODE = 4;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView)findViewById(R.id.tv);

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                tv.setText("Counter="+intent.getIntExtra("counter", 0));
            }
        };
        IntentFilter filter = new IntentFilter("sdu.myaction");
        registerReceiver(br, filter);
    }

    public void start(View v){
//        PendingIntent pi = createPendingResult(REQUEST_CODE, new Intent(), 0);

        Intent intent = new Intent("sdu.myservice");
        intent.setPackage("kz.askar.servicegroup2");
        intent.putExtra("till", 30);
        intent.putExtra("time", 1000);
//        intent.putExtra("pi", pi);
        startService(intent);

//        Intent intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 30);
//        intent.putExtra("time", 1000);
//        startService(intent);

//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 10);
//        intent.putExtra("time", 1000);
//        startService(intent);
//
//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 5);
//        intent.putExtra("time", 1000);
//        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            switch (resultCode){
                case 0:
                    tv.setText("Pending intent has sent");
                    break;
                case 1:
                    tv.setText("Counter="+data.getIntExtra("counter", 0));
                    break;
                case 2:

                    break;
            }
        }
    }

    public void stop(View v){
        stopService(new Intent(this, MyService.class));
    }
}
