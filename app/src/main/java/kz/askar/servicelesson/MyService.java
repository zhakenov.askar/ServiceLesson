package kz.askar.servicelesson;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyService extends Service {

    ExecutorService exec;
    boolean stop = false;
    PendingIntent pi;

    public MyService() {
        Log.d("My", "Service - constructor");
        exec = Executors.newFixedThreadPool(3);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("My", "Service - onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("My", "Service - onStartCommand "+startId);
        if(flags == START_FLAG_RETRY){
            Log.d("My", "Retry");
        }else if (flags == START_FLAG_REDELIVERY){
            Log.d("My", "Redelivery");
        }

        int tillInt = intent.getIntExtra("till", 0);
        int timeInt = intent.getIntExtra("time", 0);
//        pi = intent.getParcelableExtra("pi");
//        try {
//            pi.send(0);
//        } catch (PendingIntent.CanceledException e) {
//            e.printStackTrace();
//        }

        final int taskId = startId;
        final int till = tillInt;
        final int time = timeInt;
        exec.execute(new Runnable() {
            @Override
            public void run() {
                task(taskId, till, time);
            }
        });
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        Log.d("My", "Service - onDestroy");
        exec.shutdown();
        stop = true;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void task(int taskId, int till, int time){
        for(int i=0;i<till;i++){
            if(stop){
                break;
            }
            Log.d("My", "Service task #"+taskId+"="+i);

            Intent intent = new Intent("sdu.action.reciever");
            intent.putExtra("counter", i);

            //for broadcast
            sendBroadcast(intent);

//            //for PendingIntent
//            try {
//                pi.send(MyService.this, 1, intent);
//            } catch (PendingIntent.CanceledException e) {
//                e.printStackTrace();
//            }
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        stopSelf();
    }
}
