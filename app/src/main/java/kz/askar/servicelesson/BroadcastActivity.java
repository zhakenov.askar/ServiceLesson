package kz.askar.servicelesson;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class BroadcastActivity extends AppCompatActivity {

    final int REQUEST_CODE = 5;
    TextView tv;

    BroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                tv.setText("Counter:"+intent.getIntExtra("counter", 0));
            }
        };
        IntentFilter filter = new IntentFilter("sdu.action.reciever");
        registerReceiver(br, filter);
    }

    public void start(View view){
        Intent intent = new Intent("sdu.myservice");
        intent.setPackage("kz.askar.servicelesson");
        intent.putExtra("till", 10);
        intent.putExtra("time", 1000);
        startService(intent);


//        Intent intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 50);
//        intent.putExtra("time", 1000);
//        startService(intent);

//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 5);
//        intent.putExtra("time", 500);
//        startService(intent);
//
//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 2);
//        intent.putExtra("time", 1000);
//        startService(intent);
    }

    public void stop(View view){
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
    }
}
