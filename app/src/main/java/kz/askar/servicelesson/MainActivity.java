package kz.askar.servicelesson;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final int REQUEST_CODE = 5;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv);
    }

    public void start(View view){
        PendingIntent pi = this.createPendingResult(REQUEST_CODE, new Intent(), 0);

        Intent intent = new Intent("sdu.myservice");
        intent.setPackage("kz.askar.servicelesson");
        intent.putExtra("till", 10);
        intent.putExtra("time", 1000);
        intent.putExtra("pi", pi);
        startService(intent);


//        Intent intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 50);
//        intent.putExtra("time", 1000);
//        startService(intent);

//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 5);
//        intent.putExtra("time", 500);
//        startService(intent);
//
//        intent = new Intent(this, MyService.class);
//        intent.putExtra("till", 2);
//        intent.putExtra("time", 1000);
//        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE){
            if(resultCode == 0){
                tv.setText("Service connected");
            }
            if(resultCode==1){
                tv.setText("Counter:"+data.getIntExtra("counter", 0));
            }
        }
    }

    public void stop(View view){
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
    }
}
