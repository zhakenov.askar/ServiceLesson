package kz.askar.binder2;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by Zhakenov on 2/25/2017.
 */

public class MyService extends Service{

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("My", "Service onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("My", "Service onStart");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("My", "Service onBind");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("My", "Service onUnbind");
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d("My", "Service onRebind");
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("My", "Service onDestroy");
        super.onDestroy();
    }

    class MyBinder extends Binder{
        public MyService getService(){
            return MyService.this;
        }
    }


    public void startTask(String action, String message){
        Intent result = new Intent(action);
        result.putExtra("msg", message);
        sendBroadcast(result);

        NotificationCompat.Builder notifBuilder =
                new NotificationCompat.Builder(MyService.this)
                        .setSmallIcon(android.R.drawable.stat_notify_chat)
                        .setContentTitle("Service Lesson")
                        .setContentText(message)
                        .setAutoCancel(true);
        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, notifBuilder.build());
    }
}
