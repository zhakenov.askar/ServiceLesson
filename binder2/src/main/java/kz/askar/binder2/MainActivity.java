package kz.askar.binder2;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Intent intent;
    ServiceConnection sConn;

    boolean binded = false;
    MyService service;

    EditText actionET;
    EditText msgET;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionET = (EditText)findViewById(R.id.actionET);
        msgET = (EditText)findViewById(R.id.msgET);
        tv = (TextView)findViewById(R.id.tv);

        intent = new Intent(this, MyService.class);
        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("My", "From Activity service connected");
                binded = true;

                service = ((MyService.MyBinder)binder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d("My", "From Activity service disconnected");
                binded = false;
            }
        };

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                tv.setText(intent.getStringExtra("msg"));
            }
        };
        IntentFilter filter = new IntentFilter("sdu.action");
        registerReceiver(br, filter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bind(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbind(null);
    }

    public void start(View v){
        startService(intent);
    }

    public void bind(View v){
        bindService(intent, sConn, BIND_AUTO_CREATE);
    }

    public void unbind(View v){
        if(binded){
            unbindService(sConn);
            binded = false;
        }
    }

    public void stop(View v){
        stopService(intent);
    }



    public void task(View v){
        if(binded){
            service.startTask(
                    actionET.getText().toString(),
                    msgET.getText().toString());
        }
    }
}
